/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 * DTO que modeliza la tabla "cuarteles". Definido como Entity de Hibernate
 * @author Leandro
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Leandro
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "cuarteles")
public class CuartelDTO {

    @Id
    @Column(name = "Codigo", unique = true, nullable = false)
    private Integer codigo;
    
    @Column(name = "Nombre", unique = false, nullable = false)
    private String nombre;
    
    @Column(name = "Ubicacion", unique = false, nullable = false)
    private String ubicacion;

    public CuartelDTO(int codigo, String nombre, String ubicacion) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
    }

    public CuartelDTO() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public String toString() {
        return "Cuartel{" + "codigo=" + codigo + ", nombre=" + nombre + ", ubicacion=" + ubicacion + '}';
    }
}
