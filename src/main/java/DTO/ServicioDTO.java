/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * DTO que modeliza la tabla "servicios". Definido como Entity de Hibernate.
 * @author Leandro
 */
@Entity
@Table(name = "servicios")
public class ServicioDTO {

    @Id    
    @Column(name = "Codigo", unique = true, nullable = false)
    private Integer codigo;
    
    @Column(name = "Descripcion", unique = false, nullable = false)
    private String descripcion;

    public ServicioDTO(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;

    }

    public ServicioDTO() {

    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Servicio{" + "codigo=" + codigo + ", descripcion=" + descripcion + '}';
    }
}
