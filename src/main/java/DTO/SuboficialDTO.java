/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * DTO que modeliza los suboficiales dentro de la tabla "militares".
 * Definido como Entity de Hibernate.
 * @author Leandro
 */
@Entity
@DiscriminatorValue(value="Suboficial") 
public class SuboficialDTO extends MilitarDTO{

    public SuboficialDTO(Integer codigo, String tipo, String password, String nombre, String apellido, String graduacion) {
        super(codigo, tipo, password, nombre, apellido, graduacion);
    }

    public SuboficialDTO() {
    }
    
}
