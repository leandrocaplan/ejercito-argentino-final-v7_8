/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 *DTO que modeliza la tabla "companias". Definido como Entity de Hibernate
 * @author Leandro
 */
@Entity
@Table(name = "companias")
public class CompaniaDTO {
        @Id
        @Column(name = "Codigo", unique = true, nullable = false)
        private Integer codigo;
        
        @Column(name = "Actividad", unique = false, nullable = false)
        private String actividad;

        public CompaniaDTO(Integer codigo, String actividad) {
            this.codigo = codigo;
            this.actividad = actividad;
        }

        public CompaniaDTO() {
        }

        public Integer getCodigo() {
            return codigo;
        }

        public void setCodigo(int codigo) {
            this.codigo = codigo;
        }

        public String getActividad() {
            return actividad;
        }

        public void setActividad(String actividad) {
            this.actividad = actividad;
        }

        @Override
        public String toString() {
            return "Compania{" + "codigo=" + codigo + ", actividad=" + actividad + '}';
        }
    }
