/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.GenericGenerator;

/**
 * DTO que modeliza la tabla "serviciosrealizados.". 
 * Definido como Entity de Hibernate
 * @author Leandro
 */
@Entity
@Table(name = "serviciosrealizados")
public class ServicioRealizadoDTO {

    @Id
    @GenericGenerator(name = "kaugen", strategy = "increment")
    @GeneratedValue(generator = "kaugen")
    @Column(name = "CodigoServicioRealizado", unique = true, nullable = false)
    private Integer codServicioRealizado;

    @Column(name = "CodigoSoldado", unique = false, nullable = false)
    private Integer codSoldado;

    @Column(name = "CodigoServicio", unique = false, nullable = false)
    private Integer codServicio;

    @Column(name = "Fecha", unique = false, nullable = false)
    private String fecha;

    public ServicioRealizadoDTO(Integer codServicioRealizado, Integer codSoldado, Integer codServicio, String fecha) {
        this.codServicioRealizado = codServicioRealizado;
        this.codSoldado = codSoldado;
        this.codServicio = codServicio;
        this.fecha = fecha;
    }

    public ServicioRealizadoDTO() {
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getCodServicioRealizado() {
        return codServicioRealizado;
    }

    public void setCodServicioRealizado(Integer codServicioRealizado) {
        this.codServicioRealizado = codServicioRealizado;
    }

    public Integer getCodSoldado() {
        return codSoldado;
    }

    public void setCodSoldado(Integer codSoldado) {
        this.codSoldado = codSoldado;
    }

    public Integer getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(Integer codServicio) {
        this.codServicio = codServicio;
    }

    @Override
    public String toString() {
        return "ServicioRealizado{" + "codServicioRealizado=" + codServicioRealizado + ", codSoldado=" + codSoldado + ", codServicio=" + codServicio + ", fecha=" + fecha + '}';
    }

}
