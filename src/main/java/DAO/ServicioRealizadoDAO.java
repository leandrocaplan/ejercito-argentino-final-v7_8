/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.ServicioRealizadoDTO;
import controlador.*;
import static java.lang.Math.random;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JOptionPane;
import DTO.MilitarDTO;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 *
 * @author Leandro
 */
@Component
public class ServicioRealizadoDAO {

    /**
     * Declaro un atributo de tipo ArrayList <ServicioRealizadoDTO>, en el que cargaré el
     * contenido de la tabla "serviciosRealizados"
     */
    private ArrayList<ServicioRealizadoDTO> serviciosRealizados;

    /**
     * Este método carga el contenido de la tabla "serviciosRealizados" en el atributo
     * "serviciosRealizados"
     */
    public void cargarServiciosRealizados() {
        try {
//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
            this.serviciosRealizados = new ArrayList<ServicioRealizadoDTO>();

            // ////JOptionhowMessageDialog(null, query);
            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("from ServicioRealizadoDTO");
            this.serviciosRealizados = (ArrayList) query.list();
            //JOptionPane.showMessageDialog(null, this.serviciosRealizados);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
/**
 * Este método recibe por parámetro el código de soldado, el código de servicio, y una fecha para asociarlos
 * entre sí. Debido a que el ID principal del ServicioRealizadoDTO es auto-incremental, no hace falta recibirlo
 * por parámetro (es transparente para el usuario). A su constructor, podemos pasarle "null".
 * @param codSoldado
 * @param codServicio
 * @param fecha 
 */
    public void asignarServicio(String codSoldado, String codServicio, String fecha) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();

            ServicioRealizadoDTO nuevoServicioRealizado = new ServicioRealizadoDTO(null,
                    Integer.parseInt(codSoldado),
                    Integer.parseInt(codServicio), 
                    fecha);
            //JOptionPane.showMessageDialog(null, nuevoServicioRealizado);
            session.saveOrUpdate(nuevoServicioRealizado);
            //JOptionPane.showMessageDialog(null, this.serviciosRealizados);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    /**
     * Este método recibe por parámetro el código correspondiente al servicio realizado
     * a dar de baja, y lo elimina de la tabla. No hay posibilidad de que se
     * ingrese un codigo inexistente en la tabla, ya que desde la vista HTML el
     * usuario selecciona desde un menú desplegable el servicio realizado que quiere dar de
     * baja, donde solo puede seleccionar un servicio realizado ya existente.
     
     * @param codigo 
     */
    public void desasignarServicio(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from ServicioRealizadoDTO where codServicioRealizado=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null, query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Getter del atributo serviciosRealizados
     * @return 
     */
    public ArrayList<ServicioRealizadoDTO> getServiciosRealizados() {
        return serviciosRealizados;
    }
}
