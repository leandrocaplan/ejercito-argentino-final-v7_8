/*
 */
package DAO;

import DTO.CompaniaDTO;
import controlador.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import DTO.MilitarDTO;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 * DAO utilizado para administrar las companías
 *
 * @author Leandro
 */
@Component
public class CompaniaDAO {

    /**
     * Declaro un atributo de tipo ArrayList <CompaniaDTO>, en el que cargaré el
     * contenido de la tabla "companias"
     */
    private ArrayList<CompaniaDTO> companias;

    /**
     * Este método carga el contenido de la tabla "companias" en el atributo
     * "companias"
     */
    public void cargarCompanias() {
        try {
//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
            this.companias = new ArrayList<CompaniaDTO>();

            // ////JOptionhowMessageDialog(null, query);
            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("from CompaniaDTO");
            this.companias = (ArrayList) query.list();
            //JOptionPane.showMessageDialog(null, this.companias);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Este método recibe por parámetro un código y una actividad, y las inserta
     * en un nuevo registro de la tabla "companias", de no existir una con el
     * codigo ingresado. En el otro caso, actualizará un registro ya existente.
     *
     * @param codigo
     * @param actividad
     */
    public void ingresarCompania(String codigo, String actividad) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            CompaniaDTO nuevaCompania = new CompaniaDTO(Integer.parseInt(codigo), actividad);
            //JOptionPane.showMessageDialog(null, nuevaCompania);
            session.saveOrUpdate(nuevaCompania);
            //JOptionPane.showMessageDialog(null, this.companias);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Este método recibe por parámetro el código correspondiente a la companía
     * a dar de baja, y lo elimina de la tabla. No hay posibilidad de que se
     * ingrese un codigo inexistente en la tabla, ya que desde la vista HTML el
     * usuario selecciona desde un menú desplegable la companía que quiere dar
     * de baja, donde solo puede seleccionar una compania ya existente.
     *
     * @param codigo
     */
    public void bajaCompania(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from CompaniaDTO where codigo=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null,query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Getter del atributo "companias"
     * @return 
     */
    public ArrayList<CompaniaDTO> getCompanias() {
        return companias;
    } 
}
