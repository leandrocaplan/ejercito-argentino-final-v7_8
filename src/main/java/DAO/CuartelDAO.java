/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.CuartelDTO;
import controlador.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import DTO.MilitarDTO;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 * DAO utilizado para administrar los cuarteles
 *
 * @author Leandro
 */
@Component
public class CuartelDAO {

    /**
     * Declaro un atributo de tipo ArrayList <CuartelesDTO>, en el que cargaré
     * el contenido de la tabla "companias"
     */
    private ArrayList<CuartelDTO> cuarteles;

    /**
     * Este método carga el contenido de la tabla "cuarteles" en el atributo
     * "cuarteles"
     */
    public void cargarCuarteles() {

//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
        this.cuarteles = new ArrayList<CuartelDTO>();

        // ////JOptionhowMessageDialog(null, query);
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("from CuartelDTO");
        this.cuarteles = (ArrayList) query.list();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Este método recibe por parámetro un código, un nombre y una ubicacion, y
     * las inserta en un nuevo registro de la tabla "cuarteles" de no existir
     * una con el codigo ingresado. En el otro caso caso, se actualizará un
     * registro ya existente.
     *
     * @param codigo
     * @param nombre
     * @param ubicacion
     */
    public void ingresarCuartel(String codigo, String nombre, String ubicacion) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            CuartelDTO nuevoCuartel = new CuartelDTO(Integer.parseInt(codigo), nombre, ubicacion);
            //JOptionPane.showMessageDialog(null, nuevoCuartel);
            session.saveOrUpdate(nuevoCuartel);
            //JOptionPane.showMessageDialog(null, this.cuarteles);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Este método recibe por parámetro el código correspondiente al cuartel a
     * dar de baja, y lo elimina de la tabla. No hay posibilidad de que se
     * ingrese un codigo inexistente en la tabla, ya que desde la vista HTML el
     * usuario selecciona desde un menú desplegable el cuartel que quiere dar de
     * baja, donde solo puede seleccionar un cuartel ya existente.
     *
     * @param codigo
     */
    public void bajaCuartel(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from CuartelDTO where codigo=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null, query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Getter del atributo "cuarteles"
     */
    public ArrayList<CuartelDTO> getCuarteles() {
        return cuarteles;
    }

}
