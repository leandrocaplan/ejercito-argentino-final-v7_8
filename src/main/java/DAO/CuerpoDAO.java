/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.CuerpoDTO;
import controlador.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 *
 * DAO utilizado para administrar los cuerpos
 *
 * @author Leandro
 */
@Component
public class CuerpoDAO {

    /**
     * Declaro un atributo de tipo ArrayList <CuerpoDTO>, en el que cargaré el
     * contenido de la tabla "cuerpos"
     */
    private ArrayList<CuerpoDTO> cuerpos;

    /**
     * Este método carga el contenido de la tabla "cuerpos" en el atributo
     * "cuerpos"
     */
    public void cargarCuerpos() {

//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
        this.cuerpos = new ArrayList<CuerpoDTO>();

        // ////JOptionhowMessageDialog(null, query);
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("from CuerpoDTO");
        this.cuerpos = (ArrayList) query.list();
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Este método recibe por parámetro un código y una denominacion, y las
     * inserta en un nuevo registro de la tabla "cuerpos" de no existir un
     * registro con el codigo ingresado. En el otro caso caso,se actualizará un
     * registro ya existente.
     *
     * @param codigo
     * @param denominacion
     */
    public void ingresarCuerpo(String codigo, String denominacion) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            CuerpoDTO nuevoCuerpo = new CuerpoDTO(Integer.parseInt(codigo), denominacion);
            //JOptionPane.showMessageDialog(null, nuevoCuerpo);
            session.saveOrUpdate(nuevoCuerpo);
            //JOptionPane.showMessageDialog(null, this.cuerpos);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Este método recibe por parámetro el código correspondiente al cuerpo a
     * dar de baja, y lo elimina de la tabla. No hay posibilidad de que se
     * ingrese un codigo inexistente en la tabla, ya que desde la vista HTML el
     * usuario selecciona desde un menú desplegable el cuerpo que quiere dar de
     * baja, donde solo puede seleccionar un cuerpo ya existente.
     *
     * @param codigo
     */
    public void bajaCuerpo(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from CuerpoDTO where codigo=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null, query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Getter del atributo "cuerpos"
     */
    public ArrayList<CuerpoDTO> getCuerpos() {
        return cuerpos;
    }

}
